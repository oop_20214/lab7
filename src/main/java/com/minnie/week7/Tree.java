package com.minnie.week7;

public class Tree {
    private String name;;
    private int x;
    private int y;

    public Tree(String name , int x , int y) {
        this.name = name;
        this.x = x ;
        this.y = y ;

    }
    public void PrintTree() {
        System.out.println("Name = " + name );
        System.out.println(" X = " + x);
        System.out.println(" Y = " + y);
    }
    public String getname() {
        return name;
    }
    public int getx() {
        return x;
    }
    public int gety() {
        return y;
    }

    
}
