package com.minnie.week7;

public class Circle {
    private double radius;
    private double area;
    private double perimeter;

    
    public Circle(double radius) {
        this.radius = radius;
    }
    public void PrintAreaCircle1() {
        area = Math.PI * Math.pow(radius, 2);
        System.out.println("AreaCircle1 = " + area);
    }
    public void PrintPrimeterCircle1() {
        perimeter = 2 * Math.PI * radius;
        System.out.println("PrimeterCircle1 = " + perimeter);
    }
    public void PrintAreaCircle2() {
        area = Math.PI * Math.pow(radius, 2);
        System.out.println("AreaCircle2 = " + area);
    }
    public void PrintPrimeterCircle2() {
        perimeter = 2 * Math.PI * radius;
        System.out.println("PrimeterCircle2 = " + perimeter);
    }
    public double getradius() {
        return radius;
    }
    public double getarea() {
        return area;
    }
    public double getperimeter() {
        return perimeter;
    }
        

}
